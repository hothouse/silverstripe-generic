<?php

/**
 * Created by PhpStorm.
 * User: hothouse
 * Date: 24/05/16
 * Time: 1:04 PM
 */
class MetaCorporateContact extends DataObject {
	private static $db = array(
		'Telephone' => 'Varchar(255)',
		'ContactType' => 'Enum(\'customer service,technical support,billing support,bill payment,sales,reservations,credit card support,emergency,baggage tracking,roadside assistance,package tracking\',\'customer service\')',
		'AreaServed' => 'Varchar(2)',
		'ContactOption' => 'Varchar(255)',
		'AvailableLanguage' => 'Varchar(255)'
	);

	private static $summary_fields = array(
		'Telephone', 'ContactType'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->fieldByName('Root.Main.Telephone')->setRightTitle('must have international country code, i.e.: +64 3 123 4535');

		$fields->replaceField('AreaServed', TextField::create('AreaServed', 'Countries served (optional)')->setRightTitle('comma separated list of country ISO-3166 two-letter codes'));
		$fields->replaceField('ContactOption', DropdownField::create('ContactOption', 'Contact Options (optional)', array('TollFree', 'HearingImpairedSupported'))->setEmptyString('Select if applicable'));
		$fields->replaceField('AvailableLanguage', TextField::create('AvailableLanguage', 'Languages spoken (optional)')->setRightTitle('Comma separated list of languages spoken. Languages may be specified by their common English name. If omitted, the language defaults to English.'));

		return $fields;
	}
}