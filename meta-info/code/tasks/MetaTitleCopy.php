<?php
class MetaTitleCopy extends BuildTask {

	protected $title = 'Page->MetaTitle ==> SiteTree->MetaTitle';

	public function run($request) {
		$DB = Config::inst()->get('Page', 'db');
		if(isset($DB['MetaTitle'])) {
			DB::query('
				INSERT INTO SiteTree (ID, MetaTitle) SELECT ID, MetaTitle FROM Page
  				ON DUPLICATE KEY UPDATE SiteTree.MetaTitle = Page.MetaTitle
  			');
			DB::query('
				INSERT INTO SiteTree_versions (ID, MetaTitle) SELECT ID, MetaTitle FROM Page_versions
  				ON DUPLICATE KEY UPDATE SiteTree_versions.MetaTitle = Page_versions.MetaTitle
  			');
			DB::query('
				INSERT INTO SiteTree_Live (ID, MetaTitle) SELECT ID, MetaTitle FROM Page_Live
  				ON DUPLICATE KEY UPDATE SiteTree_Live.MetaTitle = Page_Live.MetaTitle
  			');
			echo 'Finished. Please remove the MetaTitle from Page::$db!';
		} else {
			echo 'No need to run db queries';
		}
	}

}