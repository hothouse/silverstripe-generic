<?php

class MetaSiteTreeExtension extends DataExtension {

	private static $db = array(
		'MetaTitle' => 'Varchar(255)'
	);

	private static $has_one = array(
		'MetaImage' => 'Image'
	);

	public function updateCMSFields(FieldList $fields) {
		$fields->addFieldToTab('Root.Main', new UploadField('MetaImage', 'Image'), 'ExtraMeta');
		$fields->addFieldToTab('Root.Main', new TextField('MetaTitle', 'Title'), 'MetaDescription');
	}

	function MetaTags(&$tags) {
		$tags .= $this->owner->renderWith('MetaTags');
	}

	public function ExtraRelTags() {
		$Tags = ArrayList::create();

		// paginated lists are to be found on the current controller
		$controller = Controller::curr();

		// paginated lists
		foreach ((array)Config::inst()->get('MetaSiteTreeExtension', 'paginated_list_sources') as $Source) {

			// don't look for objects that don't exist
			if (!$controller->hasMethod($Source)) {
				continue;
			}

			// don't look for 'Results' if no fulltext search is enabled, and if no search is triggered
			if ($Source == 'Results' && (is_null(FulltextSearchable::get_searchable_classes()) || $controller->getRequest()->latestParam('Action') != 'SearchForm')) {
				continue;
			}

			// try to get object
			$Object = null;
			try {
				if($controller->customisedObject) {
					$Object = $controller->customisedObject->obj($Source);
				} else {
					$Object = $controller->$Source();
				}
			} catch(Exception $e) {
			}

			// run through objects
			if ($Object && is_a($Object, 'PaginatedList')) {
				if ($Link = $Object->NextLink()) {
					$Tags->push(
						ArrayData::create(
							array('Rel' => 'next', 'Href' => Convert::raw2htmlatt($Link))
						)
					);
				}
				if ($Link = $Object->PrevLink()) {
					$Tags->push(
						ArrayData::create(
							array('Rel' => 'prev', 'Href' => Convert::raw2htmlatt($Link))
						)
					);
				}

				// allow only one set per page
				break;
			}
		}

		return $Tags;
	}

	public function ExtraMetaTags() {
		$Tags = ArrayList::create();

		// site config
		$SiteConfig = SiteConfig::current_site_config();

		// title
		$Title = $this->owner->MetaTitle ? $this->owner->MetaTitle : $this->owner->Title;
		$Tags->push(
			ArrayData::create(
				array('Property' => 'og:title', 'Content' => Convert::raw2htmlatt($Title))
			)
		);

		// site name
		$Tags->push(
			ArrayData::create(
				array('Property' => 'og:site_name', 'Content' => Convert::raw2htmlatt($SiteConfig->Title))
			)
		);

		// link
		$Tags->push(
			ArrayData::create(
				array('Property' => 'og:url', 'Content' => Convert::raw2htmlatt($this->owner->AbsoluteLink()))
			)
		);

		// desc
		$Description = $this->owner->MetaDescription ? $this->owner->MetaDescription :
			($this->owner->IntroText ? $this->owner->IntroText : $this->owner->obj('Content')->FirstSentence());
		if (!trim($Description)) {
			if (method_exists($this->owner, 'ContentMeta')) {
				$Description = Text::create();
				$Description->setValue($this->owner->ContentMeta());
				$Description = $Description->FirstSentence();
			}
		}
		if ($Description) {
			$Tags->push(
				ArrayData::create(
					array('Property' => 'og:description', 'Content' => Convert::raw2htmlatt($Description))
				)
			);
		}

		// image
		$Images = array();
		$ImageObjects = array();
		$twitterImages = array();
		$twitterImageSources = (array)Config::inst()->get('MetaSiteTreeExtension', 'twitter_meta_image_sources');
		foreach ((array)Config::inst()->get('MetaSiteTreeExtension', 'meta_image_sources') as $Source) {
			// reset $Object so we can start from scratch
			$Object = null;

			// $Source can be a method on the object like 'Image'
			// can also be stated as 'PageImages', 'Images.First' or even 'Media.First.Image'
			$Parts = explode('.', $Source);

			// walk through list
			$i = 0;
			foreach ($Parts as $ObjectName) {
				$i++;

				// get initial object if nothing there yet
				if (is_null($Object)) {
					if ($ObjectName == 'SiteConfig') {
						$Object = $SiteConfig;
					} elseif ($this->owner->hasMethod($ObjectName)) {
						$Object = $this->owner->$ObjectName();
					} else {
						break;
					}
				} else {
					// check also for one argument, this is not very stable ;-)
					// probably not needed anyway
					preg_match("/(.*?)\((.*?)\)/", $ObjectName, $Matches);
					$MethodArg = null;
					if (count($Matches) == 3) {
						$MethodName = $Matches[1];
						$MethodArg = $Matches[2];
					} else {
						$MethodName = $ObjectName;
					}

					// get nested object if we already have something to work with
					if ($Object->hasMethod($MethodName)) {
						$Object = $MethodArg ? $Object->$MethodName($MethodArg) : $Object->$MethodName();
					} else {
						break;
					}
				}

				// check for ancestry to determine the inheritance
				if ($Object && $Ancestry = ClassInfo::ancestry($Object)) {
					if (isset($Ancestry['Image']) && $Object->exists()) {
						// add tag if we have found an image
						$Images[] = $Object->getAbsoluteURL();
						$ImageObjects[$Object->getAbsoluteURL()] = $Object;


						if (array_search($Source, $twitterImageSources) !==false) {
							$twitterImages[] = $Object->getAbsoluteURL();
						}
					} elseif ((in_array('DataList', $Ancestry) || in_array('PaginatedList', $Ancestry) || in_array('ArrayList', $Ancestry)) && $i == count($Parts) && $Object->Count()) {
						// loop through list if this is the last item we have to process
						$Ancestry = ClassInfo::ancestry($Object->dataClass());
						$IsImageList = isset($Ancestry['Image']);
						if ($Object->dataClass() != 'ArrayData' && ($IsImageList || singleton($Object->dataClass())->hasMethod('Image'))) {
							foreach ($Object as $Item) {
								$Image = $IsImageList ? $Item : $Item->Image();
								if ($Image && $Image->exists()) {
									$Images[] = $Image->getAbsoluteURL();
									$ImageObjects[$Image->getAbsoluteURL()] = $Image;

									if (array_search($Source, $twitterImageSources) !==false) {
										$twitterImages[] = $Image->getAbsoluteURL();
									}
								}
							}
						}
					}
				}
			}
		}

		$Images = array_unique($Images);
		$Images = array_slice($Images, 0, 5);
		foreach ($Images as $Image) {
			$Tags->push(
				ArrayData::create(
					array('Property' => 'og:image', 'Content' => Convert::raw2htmlatt($Image))
				)
			);
			$object = isset($ImageObjects[$Image]) ? $ImageObjects[$Image] : null;
			if ($object) {
				$Tags->push(
					ArrayData::create(
						array('Property' => 'og:image:width', 'Content' => $object->getWidth())
					)
				);
				$Tags->push(
					ArrayData::create(
						array('Property' => 'og:image:height', 'Content' => $object->getHeight())
					)
				);
			}
		}

		// Twitter
		if ($SiteConfig->TwitterID) {
			$Tags->push(
				ArrayData::create(
					array('Property' => 'twitter:card', 'Content' => 'summary')
				)
			);
			$Tags->push(
				ArrayData::create(
					array('Property' => 'twitter:site', 'Content' => '@' . $SiteConfig->TwitterID)
				)
			);
			$Tags->push(
				ArrayData::create(
					array('Property' => 'twitter:title', 'Content' => Convert::raw2htmlatt($Title))
				)
			);
			$Tags->push(
				ArrayData::create(
					array('Property' => 'twitter:description', 'Content' => Convert::raw2htmlatt($Description))
				)
			);
			if (count($twitterImages)) {
				$image = reset($twitterImages);
				$Tags->push(
					ArrayData::create(
						array('Property' => 'twitter:image', 'Content' => Convert::raw2htmlatt($image))
					)
				);
			}
		}

		return $Tags;
	}

}