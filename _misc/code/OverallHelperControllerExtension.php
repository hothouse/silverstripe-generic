<?php

/**
 * Created by PhpStorm.
 * User: hothouse
 * Date: 26/05/16
 * Time: 2:30 PM
 */
class OverallHelperControllerExtension extends DataExtension {

	private static $allowed_actions = array(
		'StyleGuide'
	);

	public function StyleGuide() {
		if (!Director::isLive() || Permission::checkMember(Member::currentUser(), 'ADMIN')) {
			return $this->owner->RenderWith(array('StyleGuide', 'Page'));
		}
		return false;
	}
}